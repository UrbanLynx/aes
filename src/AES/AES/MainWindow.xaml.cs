﻿using System.IO;
using System.Security.Cryptography;
using System.Windows;
using AES.Model;

namespace AES
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private string fromFile;
        private string toFile;

        private void BtnFrom_OnClick(object sender, RoutedEventArgs e)
        {
            var dlg = new Microsoft.Win32.OpenFileDialog();
            var result = dlg.ShowDialog();

            if (result == true)
            {
                fromFile = dlg.FileName;
                TBfromFile.Text = fromFile;
            }
        }

        private void BtnTo_OnClick(object sender, RoutedEventArgs e)
        {
            var dlg = new Microsoft.Win32.SaveFileDialog();
            var result = dlg.ShowDialog();

            if (result == true)
            {
                toFile = dlg.FileName;
                TBtoFile.Text = toFile;
            }
        }

        private void BtnCipher_OnClick(object sender, RoutedEventArgs e)
        {
            var fileText = File.ReadAllBytes(fromFile);

            //var password = "12345678901234567890123456789012";
            var password = GetBytes(TBpassword.Text);
            ulong nonce = 1234;
            ulong i = 1;

            var encodedText = AesCounterMode.AES_CRT(fileText, password, nonce, i);
            File.WriteAllBytes(toFile, encodedText);
        }

        static byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        static string GetString(byte[] bytes)
        {
            char[] chars = new char[bytes.Length / sizeof(char)];
            System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
            return new string(chars);
        }

    }
}
