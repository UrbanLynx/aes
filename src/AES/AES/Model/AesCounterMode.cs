﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AES.Model
{
    class AesCounterMode
    {
        public static byte[] AES_CRT(byte[] input, byte[] key, ulong nonce, ulong i)
        {
            byte[] K_i;
            byte[,] out_array;
            uint numblock = (uint)(input.Length / 16);
            uint rest = (uint)(input.Length % 16);
            var aes = new AesMy();
            var w = aes.KeyExpansion(key);

            ulong temp_i = i;
            byte[] temp;
            var output = new byte[input.Length];
            for (int s = 0; s < numblock; s++)
            {
                temp = ulongToByteArray(nonce, temp_i);
                K_i = aes.Cipher(temp, w);
                for (int k = 0; k < 16; k++)
                {
                    output[s * 16 + k] = (byte)(input[s * 16 + k] ^ K_i[k]);
                }
                temp_i++;
            }

            if (rest != 0)
            {
                temp = ulongToByteArray(nonce, temp_i);
                K_i = aes.Cipher(temp, w);
                for (int k = 0; k < rest; k++)
                {
                    output[numblock * 16 + k] = (byte)(input[numblock * 16 + k] ^ K_i[k]);
                }
                temp_i++;
            }

            Array.Clear(w, 0, w.Length);

            return output;
        }

        private static byte[] ulongToByteArray(ulong nonce, ulong i)
        {
            byte[] res = new byte[16];
            for (int k = 0; k < 8; k++)
            {
                res[k] = (byte)((nonce >> 8 * k) & 0xff);
            }
            for (int k = 8; k < 16; k++)
            {
                res[k] = (byte)((i >> 8 * k) & 0xff);
            }
            return res;
        }
    }
}
